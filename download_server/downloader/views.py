from background_task import background
from django.http import FileResponse
from django.shortcuts import render

import pika
import wget

# Create your views here.
def index(request):
    response = {}
    return render(request, 'base.html', response)

def progress(request):
    response = {}
    if request.POST:
        file_url = request.POST['url']
        downloadFile(file_url)
    return render(request, 'download_progress.html', response)

def download(request):
    filename = request.GET['filename']
    file = open(filename, 'rb')
    return FileResponse(file)

# code inspired from https://medium.com/@petehouston/download-files-with-progress-in-python-96f14f6417a2 with modification
def bar_custom(current, total, width=80):
    progress_message = "Downloading: %d%% [%d / %d] bytes" % (current / total * 100, current, total)
    sendMessage(progress_message)

@background(schedule=1)
def downloadFile(url):
    filename = wget.download(url, bar=bar_custom)
    sendMessage('Access this url to view file: <a>http://152.118.148.95:20337/download/?filename=' + filename + '</a>')

# from rabbitmq tutorial https://www.rabbitmq.com/tutorials/tutorial-three-python.html
def sendMessage(message):
    routing_key = 'dummyRoutingKey123'
    credentials = pika.PlainCredentials('0806444524', '0806444524')
    connectionParameters = pika.ConnectionParameters('152.118.148.95', 5672, '/0806444524', credentials)
    connection = pika.BlockingConnection(connectionParameters)
    channel = connection.channel()
    channel.exchange_declare(exchange='1606833476', exchange_type='direct')
    channel.queue_declare(queue=routing_key)
    channel.basic_publish(exchange='1606833476', routing_key=routing_key, body=message)
    print(" [x] Sent %r:%r" % (routing_key, message))
    connection.close()
